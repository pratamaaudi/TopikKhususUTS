//
//  CalculatorBrain.swift
//  s160714027
//
//  Created by informatika02 on 9/8/17.
//  Copyright © 2017 informatika02. All rights reserved.
//

import Foundation


var rad = true;

func changeSign (input:Double) -> Double{
    return -input
}

func multiply (input1: Double, input2: Double) -> Double {
    return input1 * input2
}

func cosNya (input1: Double) -> Double {
    if(rad == true){
        return ( cos(input1) )
    } else {
        return ( cos(input1*Double.pi/180) )
    }
}

func sinNya (input1: Double) -> Double {
    if(rad == true){
        return ( sin(input1) )
    } else {
        if(input1==180){
            return 0
        } else {
            return sin(input1*Double.pi/180)
        }
    }
}

func tanNya (input1: Double) -> Double {
    if(rad == true){
        return ( tan(input1) )
    } else {
        if(input1==180){
            return 0
        } else {
            return tan(input1*Double.pi/180)
        }
    }
}

func asinNya (input1: Double) -> Double {
    if(rad == true){
        return ( asin(input1) )
    } else {
        return ( asin(input1*Double.pi/180) )
    }
}

func acosNya (input1: Double) -> Double {
    if(rad == true){
        return ( acos(input1) )
    } else {
        return ( acos(input1*Double.pi/180) )
    }
}

func atanNYA (input1: Double) -> Double {
    if(rad == true){
        return ( atan(input1) )
    } else {
        return ( atan(input1*Double.pi/180) )
    }
}

private struct PendingBinaryOperation {
    let function:(Double, Double) -> Double
    let firstOperand: Double
    
    func perform(with secondOperand: Double)->Double {
        return function (firstOperand, secondOperand)
    }
}

struct CalculatorBrain{
    
    mutating private func doRadDeg(){
        if(rad == true){
            rad = false
        } else {
            rad = true
        }
    }
    
    private var temp: PendingBinaryOperation?
    
    private var accumulator: Double?
    
    private enum Operation{
        case constant (Double)
        case unaryOperation ((Double) -> Double)
        case binaryOperation ((Double, Double) -> Double)
        case radDeg
        case equals
    }
    
    private var operations: Dictionary <String, Operation> = [
        "π" : Operation.constant (Double.pi),
        "e" : Operation.constant (M_E),
        "√" : Operation.unaryOperation (sqrt),
        "log" : Operation.unaryOperation (log),
        "sin" : Operation.unaryOperation (sinNya),
        "cos" : Operation.unaryOperation (cosNya),
        "tan" : Operation.unaryOperation (tanNya),
        "asin" : Operation.unaryOperation (asinNya),
        "acos" : Operation.unaryOperation (acosNya),
        "atan" : Operation.unaryOperation (atanNYA),
        "pmin" : Operation.unaryOperation (changeSign),
        "x" : Operation.binaryOperation(multiply),
        "=" : Operation.equals,
        "C" : Operation.constant(0),
        "RAD" : Operation.radDeg,
        "DEG" : Operation.radDeg
        
    ]
    
    
    var result: Double? {
        get{
            return accumulator
        }
    }
    
    mutating private func doPendingBinaryOperation(){
        if temp != nil && accumulator != nil {
            accumulator = temp!.perform(with: accumulator!)
            temp = nil
        }
    }
    
    mutating func doOperation (_	symbol: String){
        if let operation = operations[symbol] {
            switch operation {
            case .constant (let value):
                accumulator = value
            case .unaryOperation (let fungsiku):
                if let data = accumulator {
                    accumulator = fungsiku(data)
                }
            case .binaryOperation (let fungsiku):
                if accumulator != nil {
                    temp = PendingBinaryOperation(function: fungsiku, firstOperand: accumulator!)
                    accumulator = nil
                }
                break
            case .radDeg:
                doRadDeg()
                break
            case .equals:
                doPendingBinaryOperation()
                break
                
            }
        }
    }
    
    
    mutating func setOperation (_ operand: Double){
        accumulator = operand
    }
    
    //YANG LAMA
//    mutating func doOperation (_	symbol: String){
//        switch symbol {
//        case "C":
//            accumulator = 0
//        case "π":
//            accumulator = Double.pi
//        case "√":
//            if let operand = accumulator {
//                accumulator = sqrt(operand)
//            }
//        default:
//            print("Do nothing")
//        }
//    }
    
}
