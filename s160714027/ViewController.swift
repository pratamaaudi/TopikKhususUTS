//
//  ViewController.swift
//  s160714027
//
//  Created by informatika02 on 8/25/17.
//  Copyright © 2017 informatika02. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var userIsTyping:Bool = false
    
    private var brain:CalculatorBrain = CalculatorBrain()

    @IBOutlet weak var displayLabel: UILabel!
    
    var DisplayValue:Double {
        get {
            return Double(displayLabel.text!)!
        }
        set {
            displayLabel.text = String(newValue)
        }
    }
    
    
    func hitungJarak(dari titikA: Int, hingga titikB: Int) -> Int
    {
        let jarak = titikA - titikB
        return jarak
        
    }
    
    @IBAction func showDigit(_ sender: UIButton) {
        let digit = sender.currentTitle!
        let curentText = displayLabel.text!
        if userIsTyping {
            displayLabel.text = curentText + digit
        } else {
            displayLabel.text = digit
            userIsTyping = true
        }
        
    }
    
    @IBAction func doOperation(_ sender: UIButton) {
        //baru
        if userIsTyping{
            brain.setOperation(DisplayValue)
            userIsTyping = false
        }
        if let simbol = sender.currentTitle {
            brain.doOperation(simbol)
            if(simbol == "RAD"){
                sender.setTitle("DEG", for: UIControlState.normal)
            } else if (simbol == "DEG"){
                sender.setTitle("RAD", for: UIControlState.normal)
            }
        }
        if let result = brain.result{
            DisplayValue = result
        }
        
        //lama
//        if let simbol = sender.currentTitle {
//            switch simbol {
//            case "C":
//                displayLabel.text = "0"
//            case "√":
//                DisplayValue = sqrt(DisplayValue)
//            default:
//                print("Do nothing")
//            }
//        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

